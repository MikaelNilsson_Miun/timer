#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdlib>

#include "timer.hpp"

using namespace std;

int main()
{
    // Create randomized vector with 1000000 integers
    srand(time(nullptr));
    std::vector<int> numbers;
    numbers.resize(1000000);
    std::generate(numbers.begin(), numbers.end(), rand);

    // Create timer and mesure sorting time.
    Timer timer;
    timer.start();
    std::sort(numbers.begin(), numbers.end());
    auto nanosec =  timer.stop<std::chrono::nanoseconds>();

    cout << "Sorting 1000000 random numbers with std::sort took " << nanosec << " nanoseconds!" << endl;
    return 0;
}

