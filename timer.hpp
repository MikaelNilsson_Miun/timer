#ifndef TIMER_HPP
#define TIMER_HPP

/*
 * Autor       : Mikael Nilsson
 * Filename    : timer.hpp
 * Description : C++ 11 Timer class
 * Version     : 0.2
 *
*/

#include <chrono>

using namespace std;
using namespace chrono;

class Timer
{
public:
    Timer() : m_start(time_point<high_resolution_clock>::min())  {}
    ~Timer() = default;

    // Start timer (can be called multiple times).
    void start()
    {
        m_start = high_resolution_clock::now();
    }

    // Stop timer (can be called multiple times).
    template<typename D>
    high_resolution_clock::rep stop()
    {
        return duration_cast<D> (high_resolution_clock::now() - m_start).count();
    }

private:
    time_point<high_resolution_clock> m_start;
};

#endif // TIMER_HPP
